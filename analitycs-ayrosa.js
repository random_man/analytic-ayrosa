function showPageAnalytics()
{
    ga('send', 'pageview');
}

function sendProductsListAnalytics(listName){

     var products = $('[data-product-ayrosa]');
     for(var i = 0; i < products.length; i++) {
         var addImpression = {            // Provide product details in an impressionFieldObject.
             'id': $(products[i]).find('[data-product-id]').data('productId'),
             'name': $(products[i]).find('[data-product-name]').data('productName'),
             'category': $(products[i]).find('[data-product-category]').data('productCategory'),
             'brand': $(products[i]).find('[data-product-brand]').data('productBrand'),
             'variant': $(products[i]).find('[data-product-variant]').data('productVariant'),
             'list': listName,         // Product list (string).
             'position': i+1,                    // Product position (number).
             'price': $(products[i]).find('[data-product-price]').data('productPrice')            // Custom dimension (string).
        };
         ga('ec:addImpression', addImpression);
         console.log(addImpression);
         $(products[i]).find('[data-event-link-to-product]').click(products[i], function(eventObject) {
             var addProductClick = {// Provide product details in a productFieldObject
                 'id': $(eventObject.data).find('[data-product-id]').data('productId'),                   // Product ID (string).
                 'name': $(eventObject.data).find('[data-product-name]').data('productName'), // Product name (string).
                 'category': $(eventObject.data).find('[data-product-category]').data('productCategory'),            // Product category (string).
                 'brand': $(eventObject.data).find('[data-product-brand]').data('productBrand'),                // Product brand (string).
                 'variant': $(eventObject.data).find('[data-product-variant]').data('productVariant'),               // Product variant (string).
                 'position': i+1,                    // Product position (number).
                 'price': $(eventObject.data).find('[data-product-price]').data('productPrice'),
                 'quantity': $(eventObject.data).find('[data-product-quantity]').val()
             };
             console.log(addProductClick);
             ga('ec:addProduct', addProductClick);
             ga('ec:setAction', 'click', {       // click action.
                 'list': listName         // Product list (string).
             });
         });
         $(products[i]).find('[data-event-add-to-cart]').click(products[i], function(eventObject) {
             var addProductCart = {// Provide product details in a productFieldObject
                 'id': $(eventObject.data).find('[data-product-id]').data('productId'),                   // Product ID (string).
                 'name': $(eventObject.data).find('[data-product-name]').data('productName'), // Product name (string).
                 'category': $(eventObject.data).find('[data-product-category]').data('productCategory'),            // Product category (string).
                 'brand': $(eventObject.data).find('[data-product-brand]').data('productBrand'),                // Product brand (string).
                 'variant': $(eventObject.data).find('[data-product-variant]').data('productVariant'),               // Product variant (string).
                 'position': i+1,                    // Product position (number).
                 'price': $(eventObject.data).find('[data-product-price]').data('productPrice'),
                 'quantity': $(eventObject.data).find('[data-product-quantity]').val()
             };
             console.log(addProductCart);
             ga('ec:addProduct', addProductCart);
             ga('ec:setAction', 'add');
             ga('send', 'event', 'UX', 'click', 'add to cart');
             console.log(ga);
         });
     }
}

function sendProductCardAnalytics() {
    var product = $('[data-product-ayrosa]');
    var addProduct = {            // Provide product details in an impressionFieldObject.
            'id': $(product).find('[data-product-id]').data('productId'),
            'name': $(product).find('[data-product-name]').data('productName'),
            'category': $(product).find('[data-product-category]').data('productCategory'),
            'brand': $(product).find('[data-product-brand]').data('productBrand'),
            'variant': $(product).find('[data-product-variant]').data('productVariant'),         // Product position (number).
            'price': $(product).find('[data-product-price]').data('productPrice')
    };
    ga('ec:addProduct', addProduct);
    ga('ec:setAction', 'detail');
    $(product).find('[data-event-add-to-cart]').click(product, function(eventObject) {
        var addProduct = {// Provide product details in a productFieldObject
            'id': $(eventObject.data).find('[data-product-id]').data('productId'),                   // Product ID (string).
            'name': $(eventObject.data).find('[data-product-name]').data('productName'), // Product name (string).
            'category': $(eventObject.data).find('[data-product-category]').data('productCategory'),            // Product category (string).
            'brand': $(eventObject.data).find('[data-product-brand]').data('productBrand'),                // Product brand (string).
            'variant': $(eventObject.data).find('[data-product-variant]').data('productVariant'),               // Product variant (string).
            'price': $(eventObject.data).find('[data-product-price]').data('productPrice'),
            'quantity': $(eventObject.data).find('[data-product-quantity]').val()
        };
        console.log(addProduct);
        ga('ec:addProduct', addProduct);
        ga('ec:setAction', 'add');
        ga('send', 'event', 'UX', 'click', 'add to cart');
    });
}

function sendCheckoutStepAnalytics(stepNumber, stepOption) {
    var products = $('[data-product-ayrosa]');
    for (var i = 0; i < products.length; i++) {
        var addProduct = {            // Provide product details in an impressionFieldObject.
            'id': $(products[i]).find('[data-product-id]').data('productId'),
            'name': $(products[i]).find('[data-product-name]').data('productName'),
            'category': $(products[i]).find('[data-product-category]').data('productCategory'),
            'brand': $(products[i]).find('[data-product-brand]').data('productBrand'),
            'variant': $(products[i]).find('[data-product-variant]').data('productVariant'),
            'price': $(products[i]).find('[data-product-price]').data('productPrice'),           // Custom dimension (string).
            'quantity': $(products[i]).find('[data-product-quantity]').val()
        };
        console.log('Отправляем товар ' + addProduct + 'шаг ' + stepNumber);
        ga('ec:addProduct', addProduct);

    }
    if(stepOption == null) {
        ga('ec:setAction','checkout', { 'step': stepNumber});
        console.log('stepoption null');
    }
    else{
        ga('ec:setAction','checkout', { 'step': stepNumber, 'option': stepOption});
        console.log('stepoption not null');
    }
}


function sendCheckoutStepOptionAnalytics(stepNumber, stepOption) {
    ga('ec:setAction', 'checkout_option', {
        'step': stepNumber,
        'option': stepOption
    });
    ga('send', 'event', 'Checkout', 'Option', {
        hitCallback: function() {
            console.log('Checkoption ' + stepOption);
        }
    });
}

function sendPurchaseAnalytics(shop) {
    var products = $('[data-product-ayrosa]');
    for (var i = 0; i < products.length; i++) {
        var addProduct = {            // Provide product details in an impressionFieldObject.
            'id': $(products[i]).find('[data-product-id]').data('productId'),
            'name': $(products[i]).find('[data-product-name]').data('productName'),
            'category': $(products[i]).find('[data-product-category]').data('productCategory'),
            'brand': $(products[i]).find('[data-product-brand]').data('productBrand'),
            'variant': $(products[i]).find('[data-product-variant]').data('productVariant'),
            'price': $(products[i]).find('[data-product-price]').data('productPrice'),            // Custom dimension (string).
            'quantity': $(products[i]).find('[data-product-quantity]').data('productQuantity')
        };
        ga('ec:addProduct', addProduct);
    }
    var order = $('[data-order]');
    var purchase = {
        'id': $(order).find('[data-order-id]').data('orderId'),
        'affiliation': shop,
        'revenue': $(order).find('[data-order-revenue]').data('orderRevenue'),
        'tax': $(order).find('[data-order-tax]').data('orderTax'),
        'shipping': $(order).find('[data-order-shipping]').data('orderShipping'),
        'coupon': $(order).find('[data-order-coupon]').data('orderCoupon')    // User added a coupon at checkout.
    };
    ga('ec:setAction', 'purchase', purchase);

    console.log(purchase);
    ga('send', 'event', 'purchase', 'success', {
        hitCallback: function() {}
    });
}
